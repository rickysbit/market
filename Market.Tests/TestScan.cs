﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Market.Lib;
using System.Linq;

namespace Market.Tests
{
    [TestClass]
    public class TestScan
    {

        private PointOfSaleTerminal CreateTerminalWithPriceTable()
        {
            PriceEntity[] priceTable =
                {
                    new PriceEntity("A", 10m),
                    new PriceEntity("B", 20m),
                    new PriceEntity("C", 15m, 3, 35)
                };
            PointOfSaleTerminal terminal = new PointOfSaleTerminal();
            return terminal.SetPricing(priceTable);
        }

        private PointOfSaleTerminal CreateTerminalWithoutPriceTable()
        {
            return new PointOfSaleTerminal();
        }

        [DataTestMethod]
        [DataRow(new string[] { "A", "B", "C" })]
        public void CanAddItemHasPricing(string[] codes)
        {
            PointOfSaleTerminal terminal = CreateTerminalWithPriceTable();

            ICartItemRepository cart = terminal.Cart;
            foreach (var code in codes)
            {
                Assert.AreEqual(terminal.ScanProduct(code).CurrentItem.Code, code, 
                    "Should be able to add an item with pricing setting");
            }

            // Make sure that every code scaned is in the cart.
            Assert.IsTrue(codes.Where(p=> cart.Items.Select(i=> i.Key).Contains(p)).Count() == codes.Length,
                $"[{string.Join(",", codes)}] should be accepted and added to cart.");
        }

        [DataTestMethod]
        [DataRow("E")]
        [DataRow("G")]
        [DataRow("X")]
        public void IgnoreScanWhenCodeHasNoPrice(string code)
        {
            PointOfSaleTerminal terminal = CreateTerminalWithPriceTable();
            ICartItemRepository cart = terminal.Cart;

            var result = terminal.ScanProduct(code).CurrentItem;
            Assert.IsTrue(result == null && cart.Items.Count == 0,
                $"[{code}] should not be prime,since no pricing for it");
        }

        [DataTestMethod]
        [DataRow("A")]
        [DataRow("B")]
        [DataRow("C")]
        public void IgnoreScanWithEmptyPriceTable(string code)
        {
            PointOfSaleTerminal terminal = CreateTerminalWithoutPriceTable();

            ICartItemRepository cart = terminal.Cart;

            var result = terminal.ScanProduct(code).CurrentItem;
            Assert.IsTrue(result==null && cart.Items.Count==0, 
                $"[{code}] should not be accepted because of empty price table");
        }

    }
}
