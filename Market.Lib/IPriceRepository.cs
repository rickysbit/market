using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Lib
{
    public interface IPriceRepository
    {
        IDictionary<string, PriceEntity> priceList { get; }

        IPriceRepository Empty();

        IPriceRepository SetSinglePricing(string code, decimal unitPrice, int? volume = null, decimal? volumePrice = null);

        PriceEntity QueryPrice(string code);
    }
}
