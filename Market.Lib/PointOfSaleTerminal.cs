﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Market.Lib
{
    public class PointOfSaleTerminal
    {
        public IPriceRepository PriceTable { get; protected set; } 
        public ICartItemRepository Cart { get; protected set; }

        public CartItemEntity CurrentItem { get; protected set; }
        public PriceEntity CurrentItemPricing { get; protected set; }

        //// We may use this constructor in a real project, then we are able to use DI.
        //public PointOfSaleTerminal(IPriceRepository priceRepo, ICartItemRepository cartItemRepo)
        //{
        //    PriceTable = priceRepo;
        //    Cart = cartItemRepo;
        //    CurrentItem = null;
        //    CurrentItemPricing = null;
        //}

        public PointOfSaleTerminal()
        {
            PriceTable = new PriceRepository();
            Cart = new CartItemRepository();
            CurrentItem = null;
            CurrentItemPricing = null;
        }

        public PointOfSaleTerminal SetPricing(IList<PriceEntity> priceList)
        {
            PriceTable.Empty();
            foreach (var singlePricing in priceList)
            {
                PriceTable.SetSinglePricing(singlePricing.Code, singlePricing.UnitPrice, singlePricing.Volume, singlePricing.VolumePrice);
            }
            return this;
        }

        public PointOfSaleTerminal ScanProduct(string code)
        {
            var pricing = PriceTable.QueryPrice(code);

            // Only add the product if it has price info.
            if (pricing != null)
            {
                CurrentItem = Cart.Add(code);
                CurrentItemPricing = pricing;
            }
            return this;
        }

        public decimal CalculateTotal()
        {
            decimal total = 0m;
            foreach(var item in Cart.Items)
            {
                total += PriceTable.QueryPrice(item.Key).Caculate(item.Value.Count);
            }
            return total;
        }

        public PointOfSaleTerminal EmptyCart()
        {
            Cart.Empty();
            CurrentItem = null;
            CurrentItemPricing = null;
            return this;
        }
    }
}

