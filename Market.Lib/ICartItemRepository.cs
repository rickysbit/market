using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Lib
{
    public interface ICartItemRepository
    {
        IDictionary<string, CartItemEntity> Items { get; }

        ICartItemRepository Empty();

        CartItemEntity Add(string itemCode);
    }
}
