﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Market.Lib
{
    public class CartItemRepository: ICartItemRepository
    {
        public IDictionary<string, CartItemEntity> Items { get; protected set; }

        public CartItemRepository()
        {
            Items = new Dictionary<string, CartItemEntity>();
        }

        public ICartItemRepository Empty()
        {
            Items.Clear();
            return (ICartItemRepository)this;
        }

        public CartItemEntity Add(string itemCode)
        {
            var searchItem = Items.Where(u => u.Key == itemCode).Select(p => p.Value).SingleOrDefault();

            // If the product is already in items, simply increase the quantity. Otherwise add a new item.
            if (searchItem == null)
            {
                var newItem = new CartItemEntity(itemCode);
                Items.Add(newItem.Code, newItem);
                return newItem;
            }
            else
                searchItem.Increase();
            return searchItem;
        }

    }
}
